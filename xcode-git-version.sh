#!/bin/bash

# This script automatically sets the version and short version string of
# an Xcode project from the Git repository containing the project.

set -o errexit
set -o nounset

INFO_PLIST="${BUILT_PRODUCTS_DIR}/${WRAPPER_NAME}/Info"
VERSION=$(git log -n 1 --oneline | sed 's/[[:space:]].*//')

defaults write $INFO_PLIST CFBundleVersion $VERSION