//
//  ViewController.m
//  Beacon
//
//  Created by Brian Celenza on 2/14/14.
//  Copyright (c) 2014 Oracle. All rights reserved.
//

#import "ViewController.h"
#import "BeaconManager.h"

@interface ViewController ()

@property (nonatomic, assign) BOOL beaconRunning;

@end

@implementation ViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // register for notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beaconStartedAdvertising:) name:BeaconManagerStartedAdvertisingNotification object:nil];
    
    [self setNeedsStatusBarAppearanceUpdate];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)toggleBeacon:(id)sender
{
    if (!_beaconRunning) {
        [[BeaconManager sharedManager] startAdvertising];
    } else {
        [[BeaconManager sharedManager] stopAdvertising];
        self.beaconRunning = NO;
    }
}

#pragma mark -
#pragma mark Internal Methods
- (void)setBeaconRunning:(BOOL)beaconRunning
{
    _beaconRunning = beaconRunning;
    if (beaconRunning) {
        self.beaconStatus.text = @"Running";
        self.beaconStatus.textColor = [UIColor blueColor];
        [self.toggleButton setTitle:@"Stop Beacon" forState:UIControlStateNormal];
    } else {
        self.beaconStatus.text = @"Not Running";
        self.beaconStatus.textColor = [UIColor colorWithRed:243 green:0 blue:0 alpha:1];
        [self.toggleButton setTitle:@"Start Beacon" forState:UIControlStateNormal];
    }
}

#pragma mark -
#pragma mark Beacon Event Observers
- (void)beaconStartedAdvertising:(NSNotification *)note
{
    // look for errors
    if (note.userInfo != nil) {
        NSError *error = [note.userInfo valueForKey:@"error"];
        // code 9 is "already started," so it can be ignored
        if (error.code != 9) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            self.beaconRunning = NO;
        } else {
            self.beaconRunning = YES;
        }
    } else {
        self.beaconRunning = YES;
    }
}


@end
