//
//  BeaconManager.h
//  SpacesCX
//
//  Created by Brian Celenza on 12/4/13.
//  Copyright (c) 2013 Oracle Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ESTBeaconManager.h>

FOUNDATION_EXPORT NSString *const BeaconManagerDeterminedStateNotification;
FOUNDATION_EXPORT NSString *const BeaconManagerDiscoveredBeaconsNotification;
FOUNDATION_EXPORT NSString *const BeaconManagerEnteredRegionNotification;
FOUNDATION_EXPORT NSString *const BeaconManagerExitedRegionNotification;
FOUNDATION_EXPORT NSString *const BeaconManagerFailedDiscoveryInRegionNotification;
FOUNDATION_EXPORT NSString *const BeaconManagerRangedBeaconsNotification;
FOUNDATION_EXPORT NSString *const BeaconManagerFailedMonitoringForRegionNotification;
FOUNDATION_EXPORT NSString *const BeaconManagerFailedRangingBeaconsForRegionNotification;
FOUNDATION_EXPORT NSString *const BeaconManagerStartedAdvertisingNotification;

@interface BeaconManager : NSObject<ESTBeaconManagerDelegate>

@property (nonatomic, strong) NSMutableDictionary *regions;

@property (nonatomic, assign) BOOL isRanging;
@property (nonatomic, assign) BOOL isMonitoring;

/**
 * Returns the shared beacon manager object
 *
 * @return Shared BeaconManager object
 */
+ (BeaconManager *)sharedManager;

/**
 * Adds a new region to monitor by identifier only
 *
 * @param identifier Region identifier
 *
 * @return The initialized beacon region
 */
- (ESTBeaconRegion *)addRegionWithIdentifier:(NSString *)identifier;

/**
 * Adds a new region to monitor by identifier only
 *
 * @param identifier Region identifier
 * @param major major location value
 *
 * @return The initialized beacon region
 */
- (ESTBeaconRegion *)addRegionWithIdentifier:(NSString *)identifier major:(ESTBeaconMajorValue)major;

/**
 * Adds a new region to monitor by identifier only
 *
 * @param identifier Region identifier
 * @param major major location value
 * @param minor minor location value
 *
 * @return The initialized beacon region
 */
- (ESTBeaconRegion *)addRegionWithIdentifier:(NSString *)identifier major:(ESTBeaconMajorValue)major minor:(ESTBeaconMinorValue)minor;

/**
 * Removes a region from the shared object. Will stop monitoring it as well.
 *
 * @param identifier Region identifier
 *
 * @return void
 */
- (void)removeRegionWithIdentifier:(NSString *)identifier;

/**
 * Begins ranging for all regions assigned to this object
 *
 * @return void
 */
- (void)startRangingBeacons;

/**
 * Ends ranging for all regions assigned to this object
 *
 * @return void
 */
- (void)stopRangingBeacons;

/**
 * Begins monitoring for all regions assigned to this object
 *
 * @return void
 */
- (void)startMonitoringRegions;

/**
 * Ends monitoring for all regions assigned to this object
 *
 * @return void
 */
- (void)stopMonitoringRegions;

/**
 * Returns a localized string describing the proximity of a beacon
 *
 * @param beacon The beacon
 *
 * @return void
 */
- (NSString *)proximityDescriptionForBeacon:(ESTBeacon *)beacon;

/**
 * Starts advertising this device as a beacon
 *
 * @return void
 */
- (void)startAdvertising;

/**
 * Stops advertising this device as a beacon
 *
 * @return void
 */
- (void)stopAdvertising;

@end
