//
//  ViewController.h
//  Beacon
//
//  Created by Brian Celenza on 2/14/14.
//  Copyright (c) 2014 Oracle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITextField *beaconStatus;
@property (nonatomic, strong) IBOutlet UIButton *toggleButton;

- (IBAction)toggleBeacon:(id)sender;

@end
