//
//  AppDelegate.h
//  Beacon
//
//  Created by Brian Celenza on 2/14/14.
//  Copyright (c) 2014 Oracle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
