//
//  BeaconManager.m
//  SpacesCX
//
//  Created by Brian Celenza on 12/4/13.
//  Copyright (c) 2013 Oracle Inc. All rights reserved.
//

#import "BeaconManager.h"

static BeaconManager *beaconManager;

NSString *const BeaconManagerDeterminedStateNotification = @"BeaconManagerDeterminedState";
NSString *const BeaconManagerDiscoveredBeaconsNotification = @"BeaconManagerDiscoveredBeacons";
NSString *const BeaconManagerEnteredRegionNotification = @"BeaconManagerEnteredRegion";
NSString *const BeaconManagerExitedRegionNotification = @"BeaconManagerExitedRegion";
NSString *const BeaconManagerFailedDiscoveryInRegionNotification = @"BeaconManagerFailedDiscoveryInRegion";
NSString *const BeaconManagerRangedBeaconsNotification = @"BeaconManagerRangedBeacons";
NSString *const BeaconManagerFailedMonitoringForRegionNotification = @"BeaconManagerFailedMonitoringForRegion";
NSString *const BeaconManagerFailedRangingBeaconsForRegionNotification = @"BeaconManagerFailedRangingBeaconsForRegion";
NSString *const BeaconManagerStartedAdvertisingNotification = @"BeaconManagerStartedAdvertising";

@interface BeaconManager ()

@property (nonatomic, strong) ESTBeaconManager *estBeaconManager;

@end

@implementation BeaconManager

#pragma mark -
#pragma mark Class Methods
+ (BeaconManager *)sharedManager
{
    static dispatch_once_t beacon_manager_t;
    dispatch_once(&beacon_manager_t, ^{
        beaconManager = [[BeaconManager alloc] init];
    });
    return beaconManager;
}

#pragma mark -
#pragma mark Instance Methods
- (id)init
{
    self = [super init];
    if (self) {
        // create the local regions dict
        self.regions = [[NSMutableDictionary alloc] init];
        
        // set other defaults
        self.isRanging = NO;
        self.isMonitoring = NO;
        
        // create the internal ESTBeaconManager object
        _estBeaconManager = [[ESTBeaconManager alloc] init];
        // set ourselves as the delegate
        _estBeaconManager.delegate = self;
        // avoid unknown state beacons
        _estBeaconManager.avoidUnknownStateBeacons = YES;
    }
    return self;
}

- (ESTBeaconRegion *)addRegionWithIdentifier:(NSString *)identifier
{
    ESTBeaconRegion *region = [[ESTBeaconRegion alloc] initRegionWithIdentifier:identifier];
    [self addRegion:region];
    return region;
}

- (ESTBeaconRegion *)addRegionWithIdentifier:(NSString *)identifier major:(ESTBeaconMajorValue)major
{
    ESTBeaconRegion *region = [[ESTBeaconRegion alloc] initRegionWithMajor:major identifier:identifier];
    region.notifyEntryStateOnDisplay = YES;
    [self addRegion:region];
    return region;
}

- (ESTBeaconRegion *)addRegionWithIdentifier:(NSString *)identifier major:(ESTBeaconMajorValue)major minor:(ESTBeaconMinorValue)minor
{
    ESTBeaconRegion *region = [[ESTBeaconRegion alloc] initRegionWithMajor:major minor:minor identifier:identifier];
    region.notifyEntryStateOnDisplay = YES;
    [self addRegion:region];
    return region;
}

- (void)removeRegionWithIdentifier:(NSString *)identifier
{
    // is this a valid region?
    ESTBeaconRegion *region = [self.regions valueForKey:identifier];
    if (region != nil) {
        // make sure we aren't monitoring or ranging this region
        [_estBeaconManager stopMonitoringForRegion:region];
        [_estBeaconManager stopRangingBeaconsInRegion:region];
        
        // remove the region
        [self.regions removeObjectForKey:identifier];
    }
}

- (void)startRangingBeacons
{
    for (NSString *identifier in self.regions) {
        NSLog(@"Starting ranging for beacons in region %@", identifier);
        ESTBeaconRegion *region = [self.regions valueForKey:identifier];
        NSAssert(region != nil, @"Attempted to start ranging on a nil region");
        [_estBeaconManager startRangingBeaconsInRegion:region];
    }
    
    self.isRanging = YES;
}

- (void)stopRangingBeacons
{
    for (NSString *identifier in self.regions) {
        NSLog(@"Ending ranging for beacons in region %@", identifier);
        ESTBeaconRegion *region = [self.regions valueForKey:identifier];
        NSAssert(region != nil, @"Attempted to stop ranging on a nil region");
        [_estBeaconManager stopRangingBeaconsInRegion:region];
    }
    
    self.isRanging = NO;
}

- (void)startMonitoringRegions
{
    for (NSString *identifier in self.regions) {
        NSLog(@"Starting monitoring for beacons in region %@", identifier);
        ESTBeaconRegion *region = [self.regions valueForKey:identifier];
        NSAssert(region != nil, @"Attempted to start monitoring on a nil region");
        [_estBeaconManager startMonitoringForRegion:region];
        [_estBeaconManager requestStateForRegion:region];
    }
    
    self.isMonitoring = YES;
}

- (void)stopMonitoringRegions
{
    for (NSString *identifier in self.regions) {
        NSLog(@"Ending monitoring for beacons in region %@", identifier);
        ESTBeaconRegion *region = [self.regions valueForKey:identifier];
        NSAssert(region != nil, @"Attempted to stop monitoring on a nil region");
        [_estBeaconManager stopMonitoringForRegion:region];
    }
    
    self.isMonitoring = NO;
}

#pragma mark -
#pragma mark Internal Methods
- (void)addRegion:(ESTBeaconRegion *)region
{
    // the line below allows high frequency monitoring when the screen is on
    region.notifyEntryStateOnDisplay = YES;
    [self.regions setObject:region forKey:region.identifier];
    
    // range or monitor if the manager is already doing so
    if (self.isRanging) [_estBeaconManager startRangingBeaconsInRegion:region];
    if (self.isMonitoring) [_estBeaconManager startMonitoringForRegion:region];
}

#pragma mark -
#pragma mark Helpers
- (NSString *)proximityDescriptionForBeacon:(ESTBeacon *)beacon
{
    NSString *proximityString;
    
    switch (beacon.proximity) {
        case CLProximityImmediate:
            proximityString = NSLocalizedString(@"Immediate", @"Immediate Proximity");
            break;
            
        case CLProximityNear:
            proximityString = NSLocalizedString(@"Near", @"Near Proximity");
            break;
            
        case CLProximityFar:
            proximityString = NSLocalizedString(@"Far", @"Far Proximity");
            break;
            
        case CLProximityUnknown:
        default:
            proximityString = NSLocalizedString(@"Unknown", @"Unknown Proximity");
            break;
    }
    
    return proximityString;
}

#pragma mark -
#pragma mark ESTBeaconManagerDelegate
-(void)beaconManager:(ESTBeaconManager *)manager didDetermineState:(CLRegionState)state forRegion:(ESTBeaconRegion *)region
{
    // create the userInfo dict for notification center
    NSValue *stateValue = [NSValue value:&state withObjCType:@encode(CLRegionState)];
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:stateValue, region, nil]
                                                           forKeys:[NSArray arrayWithObjects:@"state", @"region", nil]];
    
    // create the notification
    NSNotification *notification = [[NSNotification alloc] initWithName:BeaconManagerDeterminedStateNotification
                                                                 object:manager
                                                               userInfo:userInfo];
    // notify
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

- (void)beaconManager:(ESTBeaconManager *)manager didDiscoverBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region
{
    //    NSLog(@"beaconManager didDiscoverBeacons:%@ inRegion:%@", beacons.description, region);
    // create the userInfo dict for notification center
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:beacons, region, nil]
                                                           forKeys:[NSArray arrayWithObjects:@"beacons", @"region", nil]];
    // create the notification
    NSNotification *notification = [[NSNotification alloc] initWithName:BeaconManagerDiscoveredBeaconsNotification
                                                                 object:manager
                                                               userInfo:userInfo];
    
    // notify
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

- (void)beaconManager:(ESTBeaconManager *)manager didEnterRegion:(ESTBeaconRegion *)region
{
    //    NSLog(@"beaconManager didEnterRegion:%@", region);
    // create the userInfo dict for notification center
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:region, nil]
                                                           forKeys:[NSArray arrayWithObjects:@"region", nil]];
    
    // create the notification
    NSNotification *notification = [[NSNotification alloc] initWithName:BeaconManagerEnteredRegionNotification
                                                                 object:manager
                                                               userInfo:userInfo];
    
    // notify
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

- (void)beaconManager:(ESTBeaconManager *)manager didExitRegion:(ESTBeaconRegion *)region
{
    //    NSLog(@"beaconManager didExitRegion:%@", region);
    // create the userInfo dict for notification center
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:region, nil]
                                                           forKeys:[NSArray arrayWithObjects:@"region", nil]];
    
    // create the notification
    NSNotification *notification = [[NSNotification alloc] initWithName:BeaconManagerExitedRegionNotification
                                                                 object:manager
                                                               userInfo:userInfo];
    
    // notify
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    
}

- (void)beaconManager:(ESTBeaconManager *)manager didFailDiscoveryInRegion:(ESTBeaconRegion *)region
{
    NSLog(@"WARNING: beaconManager didFailDiscoveryInRegion:%@", region);
    // create the userInfo dict for notification center
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:region, nil]
                                                           forKeys:[NSArray arrayWithObjects:@"region", nil]];
    
    // create the notification
    NSNotification *notification = [[NSNotification alloc] initWithName:BeaconManagerFailedDiscoveryInRegionNotification
                                                                 object:manager
                                                               userInfo:userInfo];
    
    // notify
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    
}

- (void)beaconManager:(ESTBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region
{
//    NSLog(@"beaconManager didRangeBeacons:%@ inRegion:%@", beacons.description, [region class]);
    // create the userInfo dict for notification center
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:beacons, region, nil]
                                                           forKeys:[NSArray arrayWithObjects:@"beacons", @"region", nil]];
    
    // create the notification
    NSNotification *notification = [[NSNotification alloc] initWithName:BeaconManagerRangedBeaconsNotification
                                                                 object:manager
                                                               userInfo:userInfo];
    
    // notify
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    
}

- (void)beaconManager:(ESTBeaconManager *)manager monitoringDidFailForRegion:(ESTBeaconRegion *)region withError:(NSError *)error
{
    NSLog(@"WARNING: beaconManager monitoringDidFailForRegion:%@ withError:%@", region, error.localizedDescription);
    // create the userInfo dict for notification center
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:region, error, nil]
                                                           forKeys:[NSArray arrayWithObjects:@"region", @"error", nil]];
    
    // create the notification
    NSNotification *notification = [[NSNotification alloc] initWithName:BeaconManagerFailedMonitoringForRegionNotification
                                                                 object:manager
                                                               userInfo:userInfo];
    
    // notify
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    
}

- (void)beaconManager:(ESTBeaconManager *)manager rangingBeaconsDidFailForRegion:(ESTBeaconRegion *)region withError:(NSError *)error
{
    NSLog(@"WARNING: beaconManager rangingBeaconsDidFailForRegion:%@ withError:%@", region, error.localizedDescription);
    // create the userInfo dict for notification center
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:region, error, nil]
                                                           forKeys:[NSArray arrayWithObjects:@"region", @"error", nil]];
    
    // create the notification
    NSNotification *notification = [[NSNotification alloc] initWithName:BeaconManagerFailedRangingBeaconsForRegionNotification
                                                                 object:manager
                                                               userInfo:userInfo];
    
    // notify
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    
}

- (void)beaconManagerDidStartAdvertising:(ESTBeaconManager *)manager error:(NSError *)error
{
    NSDictionary *userInfo;
    
    if (error) {
        NSLog(@"WARNING: beaconManagerDidStartAdvertising error:%@", error.localizedDescription);
        // create the userInfo dict for notification center
        userInfo = [[NSDictionary alloc] initWithObjects:[NSArray arrayWithObjects:error, nil]
                                                               forKeys:[NSArray arrayWithObjects:@"error", nil]];
    }
    
    
    
    // create the notification
    NSNotification *notification = [[NSNotification alloc] initWithName:BeaconManagerStartedAdvertisingNotification
                                                                 object:manager
                                                               userInfo:userInfo];
    
    // notify
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    
}

- (void)startAdvertising
{
    [_estBeaconManager startAdvertisingWithMajor:12345 withMinor:4321 withIdentifier:@"Beacon"];
}

- (void)stopAdvertising
{
    [_estBeaconManager stopAdvertising];
}

@end
